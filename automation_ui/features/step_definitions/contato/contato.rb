Dado("que esteja na página de contato") do
  autentica_login
  @menu.access_contact
  expect(@contact.title_page.text).to eql 'Contatos'
  @first_value = @contact.value_description_table.first.text
end

Quando("clicar no botão novo contato") do
  
end

Então("devo ser direcionado para o formulário de contato") do
  
end

Então("devo preencher o formulário do contato selecionando o tipo de pessoa {string} campo CNPJ {string} razão social e inscriação estadual deve vir preenchido") do |string, string2|
  
end

Então("devo informar o campo nome do representante, email, celular, telefone") do
  
end

Então("devo clicar no botão Criar Contato") do
  
end