Dado("que estou informando um usuário e uma senha inválido para realizar o login") do
  @login.login
  user_login = @login.load_data_test['user_login_invalid']
  @login.login_user(user_login)
end

Então('devo ver a mensagem {string}') do |message|
  expect(@login.message_error.text).to include message
end