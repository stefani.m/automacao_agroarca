Dado("que estou informando um usuário e uma senha para realizar o login") do
  @login.login
  user_login = @login.load_data_test['user_login']
  @login.login_user(user_login)
end
  
Quando('clico no botão Entrar') do
  @login.button_login
end

Então('devo ser direcionado para a home do agroarca') do
  expect(@home.logo_menu).to be_truthy
end

E('quando clicar no avatar') do
  @home.menu_profile.click
end

Então('clicar no botão sair') do
  @home.logout
end
  
Então('deverei ser direcionado para o login') do
  expect(@login.title_page.text).to eql "AgroARCA"
end
  