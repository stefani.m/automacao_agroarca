# language:pt

@agroarca
@contato
Funcionalidade: Cadastros > Contato
  Eu como QA do agroarca
  Quero realizar testes no menu cadastros - Contatos
  Para que eu possa validar se as funcionalidades do módulo estão corretas

  @cadastro_contato
  Cenario: Cadastro de contato
    Dado que esteja na página de contato
    Quando clicar no botão novo contato
    Então devo ser direcionado para o formulário de contato
    E devo preencher o formulário do contato selecionando o tipo de pessoa "PJ" campo CNPJ "06990590000123" razão social e inscriação estadual deve vir preenchido
    E devo informar o campo nome do representante, email, celular, telefone
    Então devo clicar no botão Criar Contato