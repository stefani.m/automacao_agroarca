# language:pt

@agroarca
@login
Funcionalidade: Login
  Eu como QA do agroarca
  Quero validar o login
  Para que eu possa certificar se as funcionalidades do login estão corretas

  @login
  Cenário: Acesso ao sistema Agroarca
    Dado que estou informando um usuário e uma senha para realizar o login
    Quando clico no botão Entrar
    Então devo ser direcionado para a home do agroarca
    E quando clicar no avatar
    E clicar no botão sair
    Então deverei ser direcionado para o login
  
  @usuario_invalido
  Cenário: Usuário ou senha inválida
    Dado que estou informando um usuário e uma senha inválido para realizar o login
    Quando clico no botão Entrar
    Então devo ver a mensagem "Ops! Houve algum problema com sua informação."
