# language:pt

@agroarca
@tipo_produto
Funcionalidade: Estoque > Tipo Produto
  Eu como QA do agroarca
  Quero realizar testes no menu estoque - Tipo Produto
  Para que eu possa validar se as funcionalidades do módulo estão corretas

  @cadastro_tipo_produto
  Cenário: Cadastro de tipo de produto
    Dado que esteja na página de tipo produto
    Quando clicar no botão novo tipo produto
    Então devo ser direcionado para o formulário de cadastro do tipo de produto
    E devo preencher a descricao do tipo de produto
    E clicar no botão Criar Tipo Produto
    Então devo ser direcionado para a listagem de tipo produto
  
  @tipo_produto_ja_existente
  Cenário: Tentar cadastrar um tipo de produto que já está cadastrado
    Dado que esteja na página de tipo produto
    Quando clicar no botão novo tipo produto
    E devo ser direcionado para o formulário de cadastro do tipo de produto
    E devo tentar realizar o cadastro de um tipo produto já existente
    Então a mensagem para o tipo de produto já existente deverá ser "A Descrição já encontra-se cadastrado na base de dados!"

  @alteracao_tipo_produto
  Cenário: Alteração de tipo de produto
    Dado que esteja na página de tipo produto
    Quando clicar no botão editar o primeiro registro da tabela tipo produto
    Então devo ser direcionado para o formulário de cadastro do tipo de produto visualizando o valor no campo descrição
    E devo alterar a descrição do tipo do produto informando o valor "alteração do tipo do produto 1"
    E clicar no botão Salvar Tipo Produto
    Então devo ser direcionado para a listagem de tipo produto visualizando o registro atualizado

  @exclusao_tipo_produto
  Cenário: Exclusão de tipo de produto
    Dado que esteja na página de tipo produto
    Quando clicar no botão excluir o primeiro registro da tabela confirmando a exclusão do tipo de produto
    Então não devo visualizar mais o registro do tipo de produto que foi excluído