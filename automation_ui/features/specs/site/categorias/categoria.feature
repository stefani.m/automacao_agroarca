# language:pt

@agroarca
@categoria
Funcionalidade: Cadastros > Categorias
  Eu como QA do agroarca
  Quero realizar testes no menu cadastros - Categorias
  Para que eu possa validar se as funcionalidades do módulo estão corretas

  @cadastro_categoria
  Cenário: Cadastro de categoria
    Dado que esteja na página de categoria
    Quando clicar no botão nova categoria
    Então devo ser direcionado para o formulário de cadastro
    E devo preencher a descricao da categoria
    E clicar no botão Criar Categoria
    Então devo ser direcionado para a listagem de categoria
  
  @categoria_ja_existente
  Cenário: Tentar cadastrar uma categoria que já está cadastrada
    Dado que esteja na página de categoria
    Quando clicar no botão nova categoria
    E devo ser direcionado para o formulário de cadastro
    E devo tentar realizar o cadastro de uma categoria já existente
    Então devo visualizar a mensagem "A Descrição já encontra-se cadastrado na base de dados!"

  @alteracao_categoria
  Cenário: Alteração de categoria
    Dado que esteja na página de categoria
    Quando clicar no botão editar o primeiro registro da tabela
    Então devo ser direcionado para o formulário de cadastro visualizando o valor no campo descrição
    E devo alterar a descrição informando o valor "alteração da descriçao"
    E clicar no botão Salvar Categoria
    Então devo ser direcionado para a listagem de categoria visualizando a categoria alterada

  @exclusao_categoria
  Cenário: Exclusão de categoria
    Dado que esteja na página de categoria
    Quando clicar no botão excluir o primeiro registro da tabela confirmando a exclusão
    Então não devo visualizar mais o registro que foi excluído