class MenuPage < SitePrism::Page

  element :menu_cadastro, 'section > ul > li:nth-child(2) > a'
  element :menu_estoque, 'section > ul > li:nth-child(3) > a'
  element :submenu_contato, 'li.treeview.active > ul > li:nth-child(1) > a > span'
  element :submenu_categorias, 'li.treeview.active > ul > li:nth-child(4) > a > span'
  element :submenu_tipo_produto, 'li.treeview.active > ul > li:nth-child(5) > a > span'
  element :submenu_contact, '.fa.fa-users'

  def access_contact
    menu_cadastro.click
    submenu_contact.click
  end

  def access_category
    menu_cadastro.click
    submenu_categorias.click
  end

  def acessa_usuarios
    
  end

  def acessa_safras

  end

  def acessa_entregas
    
  end

  def acessa_fretes

  end

  def acessa_produtos
    
  end

  def acessa_lotes

  end

  def acessa_embalagens

  end

  def acessa_marcas

  end

  def acessa_tipo_produto
    menu_estoque.click
    submenu_tipo_produto.click
  end

  def acessa_atributos_lote

  end  

  def acessa_pedidos
    
  end

  def acessa_lista_preco

  end

  def acessa_sequencias

  end

  def acessa_configuracao

  end
end