# encoding: utf-8

class CategoryPage < SitePrism::Page
  
  element  :title_page, '.box-body h1'
  element  :title_new_category, '.container-fluid h3'
  element  :input_description, '#descricao'
  elements :value_description_table, '.data-table tr td a'
  elements :btn_new_category, 'a.btn.btn-success'
  elements :edit_category, '.btn-editar'
  elements :btn_delete_category, '.btn.btn-danger.fa.fa-times'
  element  :input_description_edit, '#descricao'
  element  :btn_update_category, 'input.btn.btn-success'
  element  :btn_confirm_exclusion, '.sweet-confirm.styled'
  element  :message_duplicate, '.box-body .container-fluid'

  def new_category
    btn_new_category.last.click
  end

  def btn_create_category
    click_button 'Criar Categoria'
  end
  
  def btn_edit_category
    edit_category.first.click
  end

  def delete_category
    btn_delete_category.first.click
    sleep 1.5
    btn_confirm_exclusion.click
    sleep 1.5
    btn_confirm_exclusion.click
  end

  def save_update_category
    btn_update_category.click
  end
end
