# encoding: utf-8

class LoginPage < SitePrism::Page
  
  element :title_page, 'div.login-logo'
  element :username, 'input[name="email"]'
  element :password, 'input[name="password"]'
  element :message_error, '.alert.alert-danger'
  
  def login
    visit "/"
  end

  def load_data_test
    file = YAML.load_file(File.join(Dir.pwd, "/features/support/data/test_data.yaml"))
  end

  def login_user(user_login)
    username.set(user_login["email"])
    password.set(user_login["password"])
  end

  def button_login
    click_button 'Entrar'
  end
end
