# encoding: utf-8

class ContactPage < SitePrism::Page
  
  element  :title_page, '.box-body h1'
  element  :title_new_contact, '.container-fluid h3'
  elements :value_description_table, '.data-table tr td a'
  elements :btn_new_contact, 'a.btn.btn-success'
  elements :edit_contact, '.btn-editar'
  elements :btn_delete_contact, '.btn.btn-danger.fa.fa-times'
  element  :btn_update_contact, 'input.btn.btn-success'
  element  :btn_confirm_exclusion, '.sweet-confirm.styled'
  element  :message_duplicate, '.box-body .container-fluid'

  def new_contact
    btn_new_contact.last.click
  end

  def btn_create_contact
    click_button 'Criar Contato'
  end
  
  def btn_edit_contact
    edit_contact.first.click
  end

  def delete_contact
    btn_delete_contact.first.click
    sleep 1.5
    btn_confirm_exclusion.click
    sleep 1.5
    btn_confirm_exclusion.click
  end

  def save_update_contact
    btn_update_contact.click
  end
end
