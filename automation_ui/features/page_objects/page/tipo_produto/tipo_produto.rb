# encoding: utf-8

class TypeProductPage < SitePrism::Page
  
  element  :title_page, '.box-body h1'
  element  :title_new_type_product, '.container-fluid h3'
  element  :input_description, '#descricao'
  elements :value_description_table, '.data-table tr td a'
  elements :btn_new_type_product, 'a.btn.btn-success'
  elements :edit_type_product, '.btn-editar'
  elements :btn_delete_type_product, '.btn.btn-danger.fa.fa-times'
  element  :input_description_edit, '#descricao'
  element  :btn_update_type_product, 'input.btn.btn-success'
  element  :btn_confirm_exclusion, '.sweet-confirm.styled'
  element  :message_duplicate, '.box-body .container-fluid'

  def new_type_product
    btn_new_type_product.last.click
  end

  def btn_create_type_product
    click_button 'Criar Tipo Produto'
  end
  
  def btn_edit_type_product
    edit_type_product.first.click
  end

  def delete_type_product
    btn_delete_type_product.first.click
    sleep 1.5
    btn_confirm_exclusion.click
    sleep 1.5
    btn_confirm_exclusion.click
  end

  def save_update_type_product
    btn_update_type_product.click
  end
end
