require "base64"

Before do
  @login = LoginPage.new
  @home = HomePage.new
  @menu = MenuPage.new
  @category = CategoryPage.new
  @type_product = TypeProductPage.new
  @contact = ContactPage.new
end

def autentica_login
  step 'que estou informando um usuário e uma senha para realizar o login' 
  step 'clico no botão Entrar'
  step 'devo ser direcionado para a home do agroarca'
end